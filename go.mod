module sveltebuf

go 1.15

require (
	github.com/gobuffalo/buffalo v0.15.5
	github.com/gobuffalo/buffalo-pop/v2 v2.3.0
	github.com/gobuffalo/envy v1.9.0
	github.com/gobuffalo/flect v0.2.2 // indirect
	github.com/gobuffalo/mw-csrf v1.0.0
	github.com/gobuffalo/mw-forcessl v0.0.0-20180802152810-73921ae7a130
	github.com/gobuffalo/mw-i18n v0.0.0-20190129204410-552713a3ebb4
	github.com/gobuffalo/mw-paramlogger v0.0.0-20190129202837-395da1998525
	github.com/gobuffalo/packr/v2 v2.8.1
	github.com/gobuffalo/pop/v5 v5.3.0
	github.com/gobuffalo/suite/v3 v3.0.0
	github.com/gobuffalo/validate/v3 v3.1.0
	github.com/gobuffalo/x v0.0.0-20190224155809-6bb134105960
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/markbates/goth v1.66.1
	github.com/markbates/grift v1.5.0
	github.com/rogpeppe/go-internal v1.6.2 // indirect
	github.com/sirupsen/logrus v1.7.0 // indirect
	github.com/spf13/cobra v1.1.1 // indirect
	github.com/unrolled/secure v0.0.0-20190103195806-76e6d4e9b90c
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	golang.org/x/net v0.0.0-20201021035429-f5854403a974 // indirect
	golang.org/x/sync v0.0.0-20201020160332-67f06af15bc9 // indirect
	golang.org/x/sys v0.0.0-20210108172913-0df2131ae363 // indirect
	golang.org/x/term v0.0.0-20201210144234-2321bbc49cbf // indirect
)
