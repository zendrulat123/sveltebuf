import App from "./App.svelte";
import Nav from "./nav.svelte";

const app = new App({ target: document.body });
const nav = new Nav({ target: document.getElementById("nav") });

export default app;


