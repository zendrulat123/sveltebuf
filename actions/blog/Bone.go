package actions

import (
	"net/http"

	"github.com/gobuffalo/buffalo"
)

// Bone is a default handler to serve up

func Bone(c buffalo.Context) error {
	return c.Render(http.StatusOK, r.HTML("blog/Bone.html"))
}
